import os, sys, re

DEBUG = False
prefix = ''
offset = 5

namesequence = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
splitter = re.compile(r'(?P<num>\d+)')
folder = sys.argv[1] if len(sys.argv) > 1 else '.'
if folder[-1] != os.sep:
	folder += os.sep


def sequencer(i):
	return namesequence[offset + i//26]+namesequence[i % 26]


def sorter(key):
	name = key.rsplit('.', 1)[0]
	match = splitter.search(name)
	if match:
		return str(len(str(int(match.groupdict()['num']))))+name
	else:
		return name


def make_authorize(message='proceed ? ', response='yes'):
	done = False
	def process():
		nonlocal done
		if done:
			return True
		if input(message) == response:
			done = True
			return True
	return process
authorize = make_authorize('You are about to rename files, proceed ? ')


def first_pass():
	files = os.listdir(folder)
	files.sort(key=sorter)

	for i, name in enumerate(files):
		res = name.rsplit('.', 1)
		ext = '.'+res[1] if len(res) > 1 else ''
		newname = sequencer(i)+ext
		print('rename %s -> %s' % (folder+name, folder+newname))
		if not DEBUG and authorize():
			os.rename(folder+name, folder+newname)


def second_pass():
	files = os.listdir(folder)
	files.sort()

	for i, name in enumerate(files):
		res = name.rsplit('.', 1)
		ext = '.'+res[1] if len(res) > 1 else ''
		newname = '%s%02i%s' % (prefix, i+1, ext)
		print('rename %s -> %s' % (folder+name, folder+newname))
		if not DEBUG and authorize():
			os.rename(folder+name, folder+newname)

first_pass()
second_pass()
