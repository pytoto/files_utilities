from collections import defaultdict
import sys


class Duplicate(defaultdict):
    def __init__(self):
        defaultdict.__init__(self, list)

    def add(self, key, val):
        self[key].append(val)

    def get_dupl(self):
        return [(k, v) for k, v in self.items() if len(v) > 1]


checker = Duplicate()
with open(sys.argv[1], 'r') as f:
    for i, line in enumerate(f):
        line = line.strip()
        if line:
            checker.add(line, i+1)

print(checker.get_dupl())
