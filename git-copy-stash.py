#!/usr/bin/env python
from clingon import clingon
import os
import shutil

stash_path = os.environ.get('GC_STASH_PATH', '')  # put this into your .envrc
project_path = os.environ.get('GC_PROJECT_PATH', '')  # idem


def check_empty_parameter(param):
    if input(f"Warning no '{param}' parameter specified, proceed ? [y|(n)] ") != 'y':
        raise clingon.RunnerError("Abort")


@clingon.clize
def runner(stash=stash_path, project=project_path, subpath=""):
    """ Copy back stashed files into your project.
        Use git-copy-diff.py to stash your last commited files.py.
    """
    if not stash:
        raise clingon.RunnerError(
            "Please tell me your stash path (--stash <PATH> or export GC_STASH_PATH=<PATH>)")
    if not project:
        raise clingon.RunnerError(
            "Please tell me your project path (--project <PATH> or export GC_PROJECT_PATH=<PATH>)")
    if not os.path.exists(project):
        raise clingon.RunnerError(f"Destination {project} does not exist, aborting")
    if stash.endswith('/'):
        stash = stash[:-1]
    index = len(stash) + 1
    path = os.path.join(stash, subpath)
    if not os.path.exists(path):
        raise clingon.RunnerError(f"Stash path {path} does not exist, aborting")
    result = []
    for dirpath, dirs, files in os.walk(path):
        for file in files:
            result.append((dirpath[index:], file))

    print(f"Stash folder {path} contains {len(result)} files")

    for dir, file in result:
        src = os.path.join(stash, dir, file)
        dst = os.path.join(project, dir)
        choice = input(f"Copy {src} to {dst} ? [y|(n)|q] ")
        if choice == 'q':
            raise clingon.RunnerError("Abort")
        if choice == 'y':
            shutil.copy2(src, dst)
