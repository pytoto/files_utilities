from clingon import clingon
from collections import defaultdict
import heapq
import os
from typing import List, Tuple


KILO = 1024
MEGA = KILO * KILO


def pretty_size(size: int):
    if size < KILO:
        return size
    elif size < MEGA:
        return f"{size / KILO:.2f}K"
    else:
        return f"{size / MEGA:.2f}M"


class BiggestFiles(list):
    """ This class is used to get the N largest files of a set of pathes
    """
    def __init__(self, limit=8):
        """
        :param limit: number of files that are returned by get()
        """
        super().__init__()
        self.limit = limit
        self._result, self.size = None, 0

    def add(self, path: str) -> None:
        """ Add a new path. There is no check for duplicates.
            A path is stored as a pair (size, path),
            such that natural order reflects what we want (order on size)
        """
        self._result = None
        self.size += 1
        elt = (os.path.getsize(path), path)
        self.append(elt)

    def get(self) -> List[Tuple[int, str]]:
        """ Get the N largest files
        :return: a list of pairs (size, path)
        """
        if not self._result:
            self._result = heapq.nlargest(self.limit, self)
        return self._result


@clingon.clize
def runner(dir_exclude=[], show=[], limit=5, *directories):
    """ Count folders and distinct file extensions, showing the largest file for each extension.
        First parameter is the directry to scan (default='.').
        --dir-exclude dir1 [dir2 ...]: specify directories you don't want to explore (like .git)
        --show ext1 [ext2 ...]: will show relative pathes for these extensions with sizes,
          sorted in reverse file size (ex: --show md log)
        --limit N: size limit for the show option
    """
    if len(directories) > 1:
        raise clingon.RunnerError("You can specify only one directory")
    start_path = os.path.abspath(directories[0] if directories else '.')
    start_path_len = len(start_path) + 1
    directories = 0
    max_depth, max_depth_case = 0, start_path
    big_files = defaultdict(lambda: BiggestFiles(limit))
    show = tuple(f".{s}" if s else s for s in show)

    def scan_folder(sub_folder: str, depth: int) -> None:
        """ Scan folders recursively
        :param sub_folder: the current folder to scan
        :param depth: the current relative depth of the folder
        """
        nonlocal directories, max_depth, max_depth_case
        if depth > max_depth:
            max_depth = depth
            max_depth_case = sub_folder.path
        for entry in os.scandir(sub_folder):
            if entry.is_file():
                ext = os.path.splitext(entry.name)[-1]
                big_files[ext].add(entry.path)
            elif entry.is_dir() and entry.name not in dir_exclude:
                directories += 1
                scan_folder(entry, depth + 1)

    def file2str(file: Tuple[int, str], prefix='') -> str:
        """ Convert a file from BiggestFiles to a string for printing
        """
        size, path = file
        return f"{prefix}{path[start_path_len:]}: {pretty_size(size)}"

    scan_folder(start_path, 0)
    print(f"Folders = {directories}, Max depth = {max_depth} ({max_depth_case[start_path_len:]})\n")
    for ext in sorted(big_files):
        big = big_files[ext]
        file = big.get()[0]  # get the largest file
        # to show as path example for this extension
        print(f"{ext}: {big.size} ({file2str(file)})")
    for ext in show:
        if ext in big_files:
            if ext:
                print(f"\nWith extension '{ext}' (limit {limit}):")
            else:
                print(f"\nWithout extension (limit {limit}):")
            for file in big_files[ext].get():
                print(file2str(file, '  '))
