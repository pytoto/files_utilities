#!/usr/bin/env python

from clingon import clingon
import os
import shutil
import subprocess

stash_path = os.environ.get('GC_STASH_PATH', '../stash')  # define into your .envrc
clingon.DEBUG = True


def create_stash(path, ask=False):
    if not ask or input(f"About to create {path}, proceed ? (y|[n]) ") == 'y':
        os.makedirs(path, exist_ok=True)
        return True


def clean_stash(path):
    if input(f"About to clear {path}, proceed ? (y|[n]) ") == 'y':
        shutil.rmtree(path)
        create_stash(path)


def check_stash_empty(path):
    if os.listdir(path):
        if input(f"stash {path} is not empty, proceed ? (y|[n]) ") == 'y':
            return True


def common_prefix(files):
    prefix = os.path.commonprefix(files)
    return prefix[:prefix.rindex('/')]


def normpath(path):
    if path.startswith('/'):
        return path
    return os.path.normpath(os.path.join(os.environ['PWD'], path))


@clingon.clize
def runner(stash=stash_path, depth=0, path='', clean=False):
    """
    Copy git tracked modified files into a stash directory.
    if depth=0, copy tracked modified files (as shown by git status).
    if depth>0, copy files modified in last n commits (as shown by git diff).
    Define your stash directory via --stash or variable GC_STASH_PATH.
    Define common path to stash via --path.
    Use git-copy-stash to restore stashed files into your project.
    """
    if not stash:
        raise clingon.RunnerError(
            "Please tell me where to stash (--stash <PATH> or export GC_STASH_PATH=<PATH>)")
    stash = normpath(stash)
    if depth < 0:
        raise clingon.RunnerError("depth must be 0 or greater")
    if not os.path.exists(stash):
        if not create_stash(stash, ask=True):
            raise clingon.RunnerError(f"Stash path {stash} does not exist, aborting")
    elif clean:
        clean_stash(stash)
    elif not check_stash_empty(stash):
        print("Use option --clean or -c to clear stash")
        return
    if depth:
        git_diff = subprocess.check_output(['git', 'diff', '--name-only', f'HEAD~{depth}', 'HEAD'])
    else:
        git_diff = subprocess.check_output(['git', 'status', '-s', '-uno'])
    files = [f.decode('ascii') for f in git_diff.split() if len(f) > 2]
    if not files:
        print("No file to copy.")
        return
    prefix = common_prefix(files)
    if path:
        if prefix.endswith(path):
            prefix = prefix[:-len(path)]
            if prefix.endswith('/'):
                prefix = prefix[:-1]
        else:
            raise clingon.RunnerError(
                f"You specified common path {path} that is not in common prefix {prefix}")
    print('\n'.join(files))
    if input(f"Copy these {len(files)} file(s) to {stash} ? (y|[n]) ") != 'y':
        return

    index = len(prefix)
    files2 = [f[index:] for f in files]
    for i, file in enumerate(files2):
        try:
            source_path, source_file = file.rsplit('/', 1)
        except ValueError:
            dest_path = stash
            source_file = file
        else:
            if source_path.startswith('/'):
                source_path = source_path[1:]
            dest_path = os.path.join(stash, source_path)
            os.makedirs(dest_path, exist_ok=True)
        dest_file = os.path.join(dest_path, source_file)
        # TODO ask overwrite only if file is different
        if not os.path.isfile(dest_file) or input(f"File {dest_file} already exists, overwrite ? (y|[n]) ") == 'y':
            print(f"Copy {files[i]} to {dest_path}")
            shutil.copy2(files[i], dest_path)
